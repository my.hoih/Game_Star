import React, { Component } from 'react';
import './App.css';
import Start from './components/Start';
import Game from './components/Game';

class App extends Component {
  state = {
    isReady: true,
  }

  render() {
    return (
      <div>
        {!this.state.isReady && <Game setReady={() => this.setState({ isReady: true })} />}
        {this.state.isReady && <Start setReady={() => this.setState({ isReady: false })} />}
      </div>
    );
  }
}

export default App;

import React from 'react';
import _ from 'lodash';
import ReactMessages from 'react-messages';

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];

class Game extends React.Component {
  state = {
    next: false,
    message: '',
    icon: 'heart',
  }

  constructor() {
    super();
    const min = 1, max = 10;
    let rand = min + Math.floor((Math.random() * (max - min)));
    this.state = { random: rand, numbers: [], numbersDisable: [], reset: 5, valueOfWin: false, isLose: false, isWin: true, time: {}, seconds: 10 };
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
  }

  handleClickMessage = () => {
    this.setState({
      message: 'You lost!',
      next: true
    });
  }

  randomStar() {
    const min = 1, max = 10;
    let rand = min + Math.floor((Math.random() * (max - min)));
    this.setState({ random: rand });
    this.setState({ numbers: [] });
  }

  insertStar() {
    let stars = [];
    for (let j = 0; j < this.state.random; j++) {
      stars.push(<i className="fa fa-star fa-2x"></i>)
    }
    return stars;
  }

  handleClickReset = (e) => {
    this.randomStar();
    this.insertStar();

    const resetStar = this.state.reset;
    let reduceStar = resetStar - 1;
    this.setState({ reset: reduceStar });
    const timeResetStar = this.state.reset;
    console.log('Time of reset: ', timeResetStar);

    clearInterval(this.timer);
    this.timer = 0;
    this.setState({ seconds: 10 });
    this.startTimer();
  }

  onClickButtonNumber(e) {
    this.setState({ numbers: [...this.state.numbers, Number(e.target.value)] });
  }

  removeButtonChoice = (e) => {
    if (!e.target.value) return
    let index = this.state.numbers.indexOf(Number(e.target.value));
    this.state.numbers.splice(index, 1);

    this.setState(this.state.numbers);
    console.log('remove button:', this.state.numbers);
  }

  renderChooseButton() {
    return (
      <div>
        {!this.state.isLose && (
          <div>
            {
              this.state.numbers.map((item, index) => {
                return <button disabled={false} type="button" className="btn btn-success" value={item} onClick={this.removeButtonChoice}>{item}</button>
              })
            }
          </div>
        )}
        {this.state.isLose && (
          <div>
            {
              this.state.numbers.map((item, index) => {
                return <button disabled={true} type="button" className="btn btn-success" value={item}>{item}</button>
              })
            }
          </div>
        )}
      </div>
    );
  }

  onClickEqual = (e) => {
    let sum = 0;
    _.map(this.state.numbers, (item, index) => {
      let value = parseInt(item, 16);
      sum = sum + value;
    });

    if (sum === this.state.random) {
      this.randomStar();
      this.setState({ numbersDisable: this.state.numbersDisable.concat(this.state.numbers) });

      clearInterval(this.timer);
      this.timer = 0;
      this.setState({ seconds: 10 });
      this.startTimer();

    } else {
      this.handleClickMessage();
      this.setState({ isWin: false, isLose: true });

      clearInterval(this.timer);
    }
  }

  ReStart = () => {
    this.props.setReady();
  }

  secondsToTime(secs) {
    let hours = Math.floor(secs / (60 * 60));

    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);

    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = { h: hours, m: minutes, s: seconds };
    return obj;
  }

  componentWillMount() {
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({ time: timeLeftVar });
  }

  startTimer() {
    if (this.timer === 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds
    });
    if (this.state.seconds === 0) {
      this.setState({ isWin: false, isLose: true })
      clearInterval(this.timer);

      this.props.setReady();
    }

    let sumOfWin = 0;
    _.map(this.state.numbersDisable, (item) => {
      sumOfWin = sumOfWin + Number(item);
    });

    if (sumOfWin === 45) {
      this.setState({ valueOfWin: true })
      clearInterval(this.timer);
    }
  }

  render() {
    const { next, message, icon } = this.state;
    return (
      <div className="container">
        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          {!this.state.isLose && (
            <ReactMessages
              message={message}
              next={next}
              icon={icon}
            />
          )}
          {this.state.valueOfWin && (
            <ReactMessages
              message='You won!'
              next={true}
              icon={icon}
              duration={3000}
            />
          )}
          {this.state.isWin && (
            <div>
              {this.startTimer()}
              <span className="white">Time: {this.state.time.s}</span>
            </div>
          )}

          <br />
          <div className="row">
            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5 stars">
              {this.insertStar()}
            </div>

            <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2">
              {!this.state.isLose && (
                <button type="button" disabled={false} className="btn btn-info" onClick={e => this.onClickEqual(e)}>&#61;</button>
              )}
              {this.state.isLose && (
                <button type="button" disabled={true} className="btn btn-info">&#61;</button>
              )}

              <br />
              <br />

              {this.state.reset >= 1 && !this.state.isLose && (
                <button type="button" className="btn btn-danger" disabled={false} onClick={e => this.handleClickReset(e)}>Reset ({this.state.reset})</button>
              )}
              {this.state.reset < 1 && (
                <button type="button" className="btn btn-danger" disabled={true}>Reset</button>
              )}

              <br />
              <br />
              {this.state.isLose && (
                <button type="button" className="btn btn-success" onClick={this.ReStart()}>Play Again</button>
              )}
            </div>

            <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
              <b className="white">Your choice:</b>
              {this.renderChooseButton()}
            </div>
          </div>
        </div>

        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 number">
          {!this.state.isLose &&
            <div className="row">
              {
                _.map(array, (item, index) => {
                  {
                    let numbersArray = this.state.numbers.indexOf(item);
                    let numbersDisable = this.state.numbersDisable.indexOf(item);
                    return <button disabled={false} type="button" className="btn btn-primary" disabled={numbersArray !== -1 || numbersDisable !== -1} value={item} onClick={e => this.onClickButtonNumber(e)}>{item}</button>
                  }
                })
              }
            </div>
          }
          {this.state.isLose &&
            <div className="row">
              {
                _.map(array, (item, index) => {
                  return <button disabled={true} type="button" className="btn btn-primary" value={item}>{item}</button>
                })
              }
            </div>
          }
        </div>
      </div>
    )
  }
}

export default Game;
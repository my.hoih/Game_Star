import React from 'react';

class Start extends React.Component {
  constructor() {
    super();
    this.state = { isShownButton: true };
  }

  onClickStart = (e) => {
    this.setState({ isShownButton: !this.state.isShownButton });
    this.props.setReady();
  }

  render() {
    let shown = {
      display: this.state.isShownButton ? "block" : "none"
    };

    return (
      <div className="container">
        <h3 style={shown} className="white">Welcome to the Star Game ^^</h3>
        <button style={shown} type="button" className="btn btn-default" onClick={e => this.onClickStart(e)}>Start</button>
      </div>
    )
  }
}

export default Start;